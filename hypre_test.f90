! ================== !
!  HYPRE TEST SPACE  !
! ================== !
! Author: Venkata Krisshna
! This program is a test space for HYPRE library

program hypre_test
  
  implicit none
  include 'mpif.h'
  include 'silo_f9x.inc'
  
  integer ierr,comm
  integer, parameter :: wp=kind(1.0d0)

  ! Domain & Mesh objects
  integer irank, numproc
  integer :: e_nx,e_ny,e_nz,e_nx_,e_ny_,e_nz_
  integer :: npx,npy,npz,iproc,jproc,kproc
  real(WP) :: e_Lx,e_Ly,e_Lz
  real(WP) :: stretchx,stretchy,stretchz,bellrad
  integer, dimension(3) :: periodicity
  real(WP), dimension(:), allocatable :: e_x , e_y , e_z
  real(WP), dimension(:), allocatable :: e_xm, e_ym, e_zm
  integer  :: e_imin  , e_imax  , e_jmin  , e_jmax  , e_kmin  , e_kmax
  integer  :: e_imino , e_imaxo , e_jmino , e_jmaxo , e_kmino , e_kmaxo
  integer  :: e_imin_ , e_imax_ , e_jmin_ , e_jmax_ , e_kmin_ , e_kmax_
  integer  :: e_imino_, e_imaxo_, e_jmino_, e_jmaxo_, e_kmino_, e_kmaxo_
  integer, parameter :: e_nover=1  ! Number of ghost cells

  ! Data arrays
  real(WP), dimension(:,:,:), allocatable :: e_v

  ! HYPRE objects
  integer :: e_dim_hypre
  integer(kind=8) :: e_grid
  integer(kind=8) :: e_stencil
  integer(kind=8) :: e_matrix
  integer(kind=8) :: e_rhs
  integer(kind=8) :: e_sol
  integer(kind=8) :: e_solver
  integer(kind=8) :: e_precond
  integer, dimension(3) :: e_periodicity_hypre
  integer, dimension(3) :: e_lower,e_upper
  integer,  dimension(:), allocatable :: e_sten_ind
  real(WP), dimension(:), allocatable :: val
  integer :: e_stencil_size
  integer :: e_precond_id
  integer :: e_max_iter=10000
  real(WP) :: e_cvg=1e-7_WP
  
  ! Dirichlet BC objects
  real(WP) phi_imin,phi_imax,phi_jmin,phi_jmax,phi_kmin,phi_kmax,phi_bell
  
  call read_input
  call parallel_init
  call domain_init
  call data_init
  call silo_init
  
  if(irank.eq.1) print*,'=============== ! Program Initiated ! ==============='
  
  call hypre_init
  call hypre_solver_init
  call hypre_rhs_transfer
  call hypre_set_ig
  call hypre_solve
  call hypre_sol_transfer

  call silo_write
  
  call MPI_FINALIZE(comm,ierr)
  
contains
  
  ! ========================= !
  ! Update the HYPRE operator !
  ! ========================= !
  subroutine hypre_update
    implicit none
    integer :: i,j,k,ierr
    integer, dimension(3) :: ind
    real(WP) ddx,ddy,ddz,dnx,dny,dnz
    real(WP) w1,w2
    
    ! Build the matrix
    do k=e_kmin_,e_kmax_
       do j=e_jmin_,e_jmax_
          do i=e_imin_,e_imax_
             ind(1) = i
             ind(2) = j
             ind(3) = k
             
             ! Distances in x,y,z
             ddx = (e_xm(i+1)-e_xm(i))/(e_xm(i)-e_xm(i-1))
             dnx = (e_xm(i+1)-e_xm(i))*(e_xm(i)-e_xm(i-1))*(1+ddx)
             ddy = (e_ym(j+1)-e_ym(j))/(e_ym(j)-e_ym(j-1))
             dny = (e_ym(j+1)-e_ym(j))*(e_ym(j)-e_ym(j-1))*(1+ddy)
             ddz = (e_zm(k+1)-e_zm(k))/(e_zm(k)-e_zm(k-1))
             dnz = (e_zm(k+1)-e_zm(k))*(e_zm(k)-e_zm(k-1))*(1+ddz)
             
             ! Matrix coefficients
             val    =  0.0_WP
             val(1) = -2.0_WP*(1+ddx)/dnx
             val(2) =  2.0_WP*ddx/dnx
             val(3) =  2.0_WP/dnx
             if(e_dim_hypre.gt.1)then
                val(1) = val(1)+(-2.0_WP*(1+ddy)/dny)
                val(4) = 2.0_WP*ddy/dny
                val(5) = 2.0_WP/dny
                if(e_dim_hypre.gt.2)then
                   val(1) = val(1)+(-2.0_WP*(1+ddz)/dnz)
                   val(6) = 2.0_WP*ddz/dnz
                   val(7) = 2.0_WP/dnz
                end if
             end if
             
             ! ------------------------ RBA BCs ------------------------ !
             ! --------------------------------------------------------- !

             ! Dirichlet BCs over bell edge and bottom surface
             if (all([j.eq.e_jmin,e_xm(i).le.0]).or.j.eq.e_jmax) then
                if (i.eq.e_imin) then
                   w1 = (e_x (i)-e_xm(i-1))/(e_xm(i)-e_xm(i-1))
                   w2 = (e_xm(i)-e_x (i  ))/(e_xm(i)-e_xm(i-1))
                   val(1) = val(1)-((w1/w2)*val(2))
                   val(2) = 0.0_WP
                end if
                if (i.eq.e_imax) then
                   w1 = (e_x (i+1)-e_xm(i  ))/(e_xm(i+1)-e_xm(i))
                   w2 = (e_xm(i+1)-e_x (i+1))/(e_xm(i+1)-e_xm(i))
                   val(1) = val(1)-((w2/w1)*val(3))
                   val(3) = 0.0_WP
                end if
                if (e_dim_hypre.gt.1) then
                   if (j.eq.e_jmin) then
                      w1 = (e_y (j)-e_ym(j-1))/(e_ym(j)-e_ym(j-1))
                      w2 = (e_ym(j)-e_y (j  ))/(e_ym(j)-e_ym(j-1))
                      val(1) = val(1)-((w1/w2)*val(4))
                      val(4) = 0.0_WP
                   end if
                   if (j.eq.e_jmax) then
                      w1 = (e_y (j+1)-e_ym(j  ))/(e_ym(j+1)-e_ym(j))
                      w2 = (e_ym(j+1)-e_y (j+1))/(e_ym(j+1)-e_ym(j))
                      val(1) = val(1)-((w2/w1)*val(5))
                      val(5) = 0.0_WP
                   end if
                   if (e_dim_hypre.gt.2) then
                      if (k.eq.e_kmin) then
                         w1 = (e_z (k)-e_zm(k-1))/(e_zm(k)-e_zm(k-1))
                         w2 = (e_zm(k)-e_z (k  ))/(e_zm(k)-e_zm(k-1))
                         val(1) = val(1)-((w1/w2)*val(4))
                         val(4) = 0.0_WP
                      end if
                      if (k.eq.e_kmax) then
                         w1 = (e_z (k+1)-e_zm(k  ))/(e_zm(k+1)-e_zm(k))
                         w2 = (e_zm(k+1)-e_z (k+1))/(e_zm(k+1)-e_zm(k))
                         val(1) = val(1)-((w2/w1)*val(5))
                         val(5) = 0.0_WP
                      end if
                   end if
                end if
             end if
             
             ! Neumann BCs
             if (i.eq.e_imin) then
                val(1) = val(1)+val(2)
                val(2) = 0.0_WP
             end if
             if (i.eq.e_imax) then
                val(1) = val(1)+val(3)
                val(3) = 0.0_WP
             end if
             if (e_dim_hypre.gt.1) then
                if (j.eq.e_jmin .and. e_xm(i).gt.0) then
                   if (j.eq.e_jmin) then
                      val(1) = val(1)+val(4)
                      val(4) = 0.0_WP
                   end if
                   if (j.eq.e_jmax) then
                      val(1) = val(1)+val(5)
                      val(5) = 0.0_WP
                   end if
                   if (e_dim_hypre.gt.2) then
                      if (k.eq.e_kmin) then
                         val(1) = val(1)+val(6)
                         val(6) = 0.0_WP
                      end if
                      if (k.eq.e_kmax) then
                         val(1) = val(1)+val(7)
                         val(7) = 0.0_WP
                      end if
                   end if
                end if
             end if
             
             ! --------------------------------------------------------- !

             ! ! Dirichlet BCs
             ! ! if (i.eq.e_imin) then
             ! !    w1 = (e_x (i)-e_xm(i-1))/(e_xm(i)-e_xm(i-1))
             ! !    w2 = (e_xm(i)-e_x (i  ))/(e_xm(i)-e_xm(i-1))
             ! !    val(1) = val(1)-((w1/w2)*val(2))
             ! !    val(2) = 0.0_WP
             ! ! end if
             ! ! if (i.eq.e_imax) then
             ! !    w1 = (e_x (i+1)-e_xm(i  ))/(e_xm(i+1)-e_xm(i))
             ! !    w2 = (e_xm(i+1)-e_x (i+1))/(e_xm(i+1)-e_xm(i))
             ! !    val(1) = val(1)-((w2/w1)*val(3))
             ! !    val(3) = 0.0_WP
             ! ! end if
             ! if (e_dim_hypre.gt.1) then
             !    if (j.eq.e_jmin) then
             !       w1 = (e_y (j)-e_ym(j-1))/(e_ym(j)-e_ym(j-1))
             !       w2 = (e_ym(j)-e_y (j  ))/(e_ym(j)-e_ym(j-1))
             !       val(1) = val(1)-((w1/w2)*val(4))
             !       val(4) = 0.0_WP
             !    end if
             !    if (j.eq.e_jmax) then
             !       w1 = (e_y (j+1)-e_ym(j  ))/(e_ym(j+1)-e_ym(j))
             !       w2 = (e_ym(j+1)-e_y (j+1))/(e_ym(j+1)-e_ym(j))
             !       val(1) = val(1)-((w2/w1)*val(5))
             !       val(5) = 0.0_WP
             !    end if
             ! !    if (e_dim_hypre.gt.2) then
             ! !       if (k.eq.e_kmin) then
             ! !          w1 = (e_z (k)-e_zm(k-1))/(e_zm(k)-e_zm(k-1))
             ! !          w2 = (e_zm(k)-e_z (k  ))/(e_zm(k)-e_zm(k-1))
             ! !          val(1) = val(1)-((w1/w2)*val(4))
             ! !          val(4) = 0.0_WP
             ! !       end if
             ! !       if (k.eq.e_kmax) then
             ! !          w1 = (e_z (k+1)-e_zm(k  ))/(e_zm(k+1)-e_zm(k))
             ! !          w2 = (e_zm(k+1)-e_z (k+1))/(e_zm(k+1)-e_zm(k))
             ! !          val(1) = val(1)-((w2/w1)*val(5))
             ! !          val(5) = 0.0_WP
             ! !       end if
             ! !    end if
             ! end if

             ! ! Neumann BCs
             ! if (i.eq.e_imin) then
             !    val(1) = val(1)+val(2)
             !    val(2) = 0.0_WP
             ! end if
             ! if (i.eq.e_imax) then
             !    val(1) = val(1)+val(3)
             !    val(3) = 0.0_WP
             ! end if
             ! if (e_dim_hypre.gt.1) then
             !    ! if (j.eq.e_jmin) then
             !    !    val(1) = val(1)+val(4)
             !    !    val(4) = 0.0_WP
             !    ! end if
             !    ! if (j.eq.e_jmax) then
             !    !    val(1) = val(1)+val(5)
             !    !    val(5) = 0.0_WP
             !    ! end if
             !    if (e_dim_hypre.gt.2) then
             !       if (k.eq.e_kmin) then
             !          val(1) = val(1)+val(6)
             !          val(6) = 0.0_WP
             !       end if
             !       if (k.eq.e_kmax) then
             !          val(1) = val(1)+val(7)
             !          val(7) = 0.0_WP
             !       end if
             !    end if
             ! end if
             
             ! ! Print values
             ! if (e_dim_hypre.eq.1)&
             !      print*,'MAT VAL',val(2),val(1),val(3)
             ! if (e_dim_hypre.eq.2)&
             !      print*,'MAT VAL',val(4),val(2),val(1),val(3),val(5)
             ! if (e_dim_hypre.eq.3)&
             !      print*,'MAT VAL',val(6),val(4),val(2),val(1),val(3),val(5),val(7)
             
             call HYPRE_StructMatrixSetValues(e_matrix,ind(1:e_dim_hypre),e_stencil_size,e_sten_ind,val,ierr)
          end do
       end do
    end do
    
    call HYPRE_StructMatrixAssemble(e_matrix,ierr)
    ! call HYPRE_StructMatrixPrint(e_matrix,1,ierr)

    return
  end subroutine hypre_update
  
  ! ==================== !
  !  HYPRE RHS transfer  !
  ! ==================== !
  subroutine hypre_rhs_transfer
    implicit none
    integer :: i,j,k,ierr
    integer, dimension(3) :: ind
    real(WP) :: value
    real(16), parameter :: pi=4*atan(1.0_16)
    real(WP) ddx,ddy,ddz,dnx,dny,dnz
    real(WP) w1,w2,v2,v3

    do k = e_kmin_,e_kmax_
       do j = e_jmin_,e_jmax_
          do i = e_imin_,e_imax_
             
             ! Mesh position
             ind(1) = i
             ind(2) = j
             ind(3) = k

             ! RHS value
             value = 0.0_WP
             
             ! ---------------------------- RBA BCs ---------------------------- !
             ! ----------------------------------------------------------------- !
             ! Dirichlet BCs over bell egde and bottom surface
             if (j.eq.e_jmin .and. e_xm(i).le.0) then
                ddy = (e_ym(j+1)-e_ym(j))/(e_ym(j)-e_ym(j-1))
                dny = (e_ym(j+1)-e_ym(j))*(e_ym(j)-e_ym(j-1))*(1+ddy)
                w2  = (e_ym(j)-e_y (j  ))/(e_ym(j)-e_ym(j-1))
                w1  = (e_y (j)-e_ym(j-1))/(e_ym(j)-e_ym(j-1))
                v2  = 2.0_WP*ddy/dny
                value = value-(phi_bell*v2/w2)
             else if (j.eq.e_jmax) then
                value = value - 0.0_WP
             end if
             ! ----------------------------------------------------------------- !
             
             ! ! RHS value
             ! value = 0.0_WP
             
             ! ! ! Dirichlet BCs
             ! ! if ( i.eq.e_imin .or. i.eq.e_imax .or.&
             ! !      j.eq.e_jmin .or. j.eq.e_jmax .or.&
             ! !      k.eq.e_kmin .or. k.eq.e_kmax) then
             ! !    if(i.eq.e_imin)then
             ! !       ddx = (e_xm(i+1)-e_xm(i))/(e_xm(i)-e_xm(i-1))
             ! !       dnx = (e_xm(i+1)-e_xm(i))*(e_xm(i)-e_xm(i-1))*(1+ddx)
             ! !       w2  = (e_xm(i)-e_x (i  ))/(e_xm(i)-e_xm(i-1))
             ! !       w1  = (e_x (i)-e_xm(i-1))/(e_xm(i)-e_xm(i-1))
             ! !       v2  = 2.0_WP*ddx/dnx
             ! !       value = value-(phi_imin*v2/w2)
             ! !    end if
             ! !    if(i.eq.e_imax)then
             ! !       ddx = (e_xm(i+1)-e_xm(i))/(e_xm(i)-e_xm(i-1))
             ! !       dnx = (e_xm(i+1)-e_xm(i))*(e_xm(i)-e_xm(i-1))*(1+ddx)
             ! !       w2  = (e_xm(i+1)-e_x (i+1))/(e_xm(i+1)-e_xm(i))
             ! !       w1  = (e_x (i+1)-e_xm(i  ))/(e_xm(i+1)-e_xm(i))
             ! !       v3  = 2.0_WP/dnx
             ! !       value = value-(phi_imax*v3/w1)
             ! !    end if
             !    if (e_dim_hypre.gt.1) then
             !       if(j.eq.e_jmin)then
             !          ddy = (e_ym(j+1)-e_ym(j))/(e_ym(j)-e_ym(j-1))
             !          dny = (e_ym(j+1)-e_ym(j))*(e_ym(j)-e_ym(j-1))*(1+ddy)
             !          w2  = (e_ym(j)-e_y (j  ))/(e_ym(j)-e_ym(j-1))
             !          w1  = (e_y (j)-e_ym(j-1))/(e_ym(j)-e_ym(j-1))
             !          v2  = 2.0_WP*ddy/dny
             !          value = value-(phi_jmin*v2/w2)
             !       end if
             !       if(j.eq.e_jmax)then
             !          ddy = (e_ym(j+1)-e_ym(j))/(e_ym(j)-e_ym(j-1))
             !          dny = (e_ym(j+1)-e_ym(j))*(e_ym(j)-e_ym(j-1))*(1+ddy)
             !          w2  = (e_ym(j+1)-e_y (j+1))/(e_ym(j+1)-e_ym(j))
             !          w1  = (e_y (j+1)-e_ym(j  ))/(e_ym(j+1)-e_ym(j))
             !          v3  = 2.0_WP/dny
             !          value = value-(phi_jmax*v3/w1)
             !       end if
             !    !    if (e_dim_hypre.gt.2) then
             !    !       if(k.eq.e_kmin)then
             !    !          ddz = (e_zm(k+1)-e_zm(k))/(e_zm(k)-e_zm(k-1))
             !    !          dnz = (e_zm(k+1)-e_zm(k))*(e_zm(k)-e_zm(k-1))*(1+ddz)
             !    !          w2  = (e_zm(k)-e_z (k  ))/(e_zm(k)-e_zm(k-1))
             !    !          w1  = (e_z (k)-e_zm(k-1))/(e_zm(k)-e_zm(k-1))
             !    !          v2  = 2.0_WP*ddz/dnz
             !    !          value = value-(phi_kmin*v2/w2)
             !    !       end if
             !    !       if(k.eq.e_kmax)then
             !    !          ddz = (e_zm(k+1)-e_zm(k))/(e_zm(k)-e_zm(k-1))
             !    !          dnz = (e_zm(k+1)-e_zm(k))*(e_zm(k)-e_zm(k-1))*(1+ddz)
             !    !          w2  = (e_zm(k+1)-e_z (k+1))/(e_zm(k+1)-e_zm(k))
             !    !          w1  = (e_z (k+1)-e_zm(k  ))/(e_zm(k+1)-e_zm(k))
             !    !          v3  = 2.0_WP/dnz
             !    !          value = value-(phi_kmax*v3/w1)
             !    !       end if
             !    !    end if
             !    end if
             ! ! end if

             ! ! Print values
             ! print*,'RHS VAL',i,j,k,e_xm(i),value
             
             ! Set the values
             call HYPRE_StructVectorSetValues(e_rhs,ind(1:e_dim_hypre),value,ierr)
             
          end do
       end do
    end do

    call HYPRE_StructVectorAssemble(e_rhs,ierr)
    ! call HYPRE_StructVectorPrint(e_rhs,0,ierr)
    
    return
  end subroutine hypre_rhs_transfer

  ! ========================= !
  !  HYPRE Solution transfer  !
  ! ========================= !
  subroutine hypre_sol_transfer
    implicit none
    integer :: i,j,k,ierr
    integer, dimension(3) :: ind
    real(WP) :: value

    do k=e_kmin_,e_kmax_
       do j=e_jmin_,e_jmax_
          do i=e_imin_,e_imax_
             ind(1) = i
             ind(2) = j
             ind(3) = k
             call HYPRE_StructVectorGetValues(e_sol,ind(1:e_dim_hypre),value,ierr)
             e_v(i,j,k) = value
             ! print*,e_xm(i),e_ym(j),value
          end do
       end do
    end do
    
    if (irank.eq.2) then
       print*,'SOLUTION'
       print*,'--------'
       do j=e_jmax_,e_jmin_,-1
          print*,e_v(e_imin_:e_imax_,j,int(e_nz/2)+2)
       end do
    end if

    return
  end subroutine hypre_sol_transfer

  ! ================================= !
  !  Initiating HYPRE Poisson Solver  !
  ! ================================= !
  subroutine hypre_init
    implicit none
    integer :: count
    integer :: offsets(7,3)
    integer, dimension(3) :: offset
    integer, dimension(6) :: e_matrix_num_ghost
    
    ! Determine Hypre dimensions
    e_dim_hypre = 3
    if (e_nz.eq.1) e_dim_hypre = 2
    if (e_ny.eq.1) e_dim_hypre = 2
    if (e_ny.eq.1.and.e_nz.eq.1) e_dim_hypre = 1
    
    ! Initialize Hypre - Struct
    call HYPRE_StructGridCreate(comm,e_dim_hypre,e_grid,ierr)
    e_lower = 0; e_upper = 0
    e_lower(1) = e_imin_
    e_upper(1) = e_imax_
    if(e_dim_hypre.gt.1)then
       e_lower(2) = e_jmin_
       e_upper(2) = e_jmax_
       if(e_dim_hypre.gt.2)then
          e_lower(3) = e_kmin_
          e_upper(3) = e_kmax_
       end if
    end if
    call HYPRE_StructGridSetExtents(e_grid,e_lower(1:e_dim_hypre),e_upper(1:e_dim_hypre),ierr)
    if (periodicity(1).eq.1) e_periodicity_hypre(1) = e_nx
    if (periodicity(2).eq.1) e_periodicity_hypre(2) = e_ny
    if (periodicity(3).eq.1) e_periodicity_hypre(3) = e_nz
    call HYPRE_StructGridSetPeriodic(e_grid,e_periodicity_hypre(1:e_dim_hypre),ierr)
    call HYPRE_StructGridAssemble(e_grid,ierr)
    
   ! Build the stencil
    e_stencil_size = 2*e_dim_hypre+1
    offsets(1,:) = [0,0,0]
    offsets(2,:) = [-1,0,0]
    offsets(3,:) = [1,0,0]
    if(e_dim_hypre.gt.1)then
       offsets(4,:) = [0,-1,0]
       offsets(5,:) = [0,1,0]
       if(e_dim_hypre.gt.2)then
          offsets(6,:) = [0,0,-1]
          offsets(7,:) = [0,0,1]
       end if
    end if
    call HYPRE_StructStencilCreate(e_dim_hypre,e_stencil_size,e_stencil,ierr)
    do count=1,e_stencil_size
       offset = offsets(count,:)
       call HYPRE_StructStencilSetElement(e_stencil,count-1,offset,ierr)
    end do
    
    ! Build the matrix
    call HYPRE_StructMatrixCreate(comm,e_grid,e_stencil,e_matrix,ierr)
    e_matrix_num_ghost = 0
    call HYPRE_StructMatrixSetNumGhost(e_matrix,e_matrix_num_ghost(1:2*e_dim_hypre),ierr)
    call HYPRE_StructMatrixInitialize(e_matrix,ierr)

    ! Prepare RHS
    call HYPRE_StructVectorCreate(comm,e_grid,e_rhs,ierr)
    call HYPRE_StructVectorInitialize(e_rhs,ierr)
    call HYPRE_StructVectorAssemble(e_rhs,ierr)
    
    ! Create solution vector
    call HYPRE_StructVectorCreate(comm,e_grid,e_sol,ierr)
    call HYPRE_StructVectorInitialize(e_sol,ierr)
    call HYPRE_StructVectorAssemble(e_sol,ierr)

    ! Create stencil index
    allocate(e_sten_ind(e_stencil_size))
    allocate(val(e_stencil_size))
    do count=1,e_stencil_size
       e_sten_ind(count) = count-1
    end do

    return
  end subroutine hypre_init

  ! ========================= !
  !  Initiating HYPRE Solver  !
  ! ========================= !
  subroutine hypre_solver_init
    implicit none
    integer  :: ierr
    integer  :: log_level

    call hypre_update          ! Update the operator
    call hypre_precond_init    ! Setup the preconditioner

    ! Setup the solver
    log_level = 1
    call HYPRE_StructBICGSTABCreate(comm,e_solver,ierr)
    call HYPRE_StructBICGSTABSetMaxIter(e_solver,e_max_iter,ierr)
    call HYPRE_StructBICGSTABSetTol(e_solver,e_cvg,ierr)
    call HYPRE_StructBICGSTABSetLogging(e_solver,log_level,ierr)
    if (e_precond_id.ge.0) call HYPRE_StructBICGSTABSetPrecond(e_solver,e_precond_id,e_precond,ierr)
    call HYPRE_StructBICGSTABSetup(e_solver,e_matrix,e_rhs,e_sol,ierr)

    return
  end subroutine hypre_solver_init

  ! ========================= !
  !  HYPRE Set Initial Guess  !
  ! ========================= !
  subroutine hypre_set_ig
    implicit none
    integer :: i,j,k,ierr
    integer, dimension(3) :: ind
    real(WP) :: value

    do k=e_kmin_,e_kmax_
       do j=e_jmin_,e_jmax_
          do i=e_imin_,e_imax_
             ind(1) = i
             ind(2) = j
             ind(3) = k
             value  = 0.0_WP
             call HYPRE_StructVectorSetValues(e_sol,ind(1:e_dim_hypre),value,ierr)
          end do
       end do
    end do
    call HYPRE_StructVectorAssemble(e_sol,ierr)

    return
  end subroutine hypre_set_ig

  ! ============== !
  !  HYPRE Solver  !
  ! ============== !
  subroutine hypre_solve
    implicit none
    integer :: ierr
    integer :: e_it
    real(WP) :: e_max_res

    ! Prepare the solver
    call HYPRE_StructBICGSTABSolve(e_solver,e_matrix,e_rhs,e_sol,ierr)
    call HYPRE_StructBICGSTABGetNumItera(e_solver,e_it,ierr)
    call HYPRE_StructBICGSTABGetFinalRel(e_solver,e_max_res,ierr)
    if (irank.eq.1) then
       print *,'No  of iters =',e_it
       print *,'Max Residual =',e_max_res
    end if
    
    return
  end subroutine hypre_solve

  ! ========================================== !
  ! Initialization of the HYPRE preconditioner !
  ! ========================================== !
  subroutine hypre_precond_init
    implicit none

    e_precond_id = -1

    return
  end subroutine hypre_precond_init

  ! ====================== !
  !  Initiate Domain Grid  !
  ! ====================== !
  subroutine domain_init
    implicit none
    integer :: i,j,k
    integer :: q,r

    e_Lx = 2*bellrad

    ! Set the global indices
    e_imino = 1
    e_imin  = e_imino + e_nover
    e_imax  = e_imin  + e_nx - 1
    e_imaxo = e_imax  + e_nover
    e_jmino = 1
    e_jmin  = e_jmino + e_nover
    e_jmax  = e_jmin  + e_ny - 1
    e_jmaxo = e_jmax  + e_nover
    e_kmino = 1
    e_kmin  = e_kmino + e_nover
    e_kmax  = e_kmin  + e_nz - 1
    e_kmaxo = e_kmax  + e_nover

    ! Partitioning in X
    q = e_nx/npx
    r = mod(e_nx,npx)
    if (iproc<=r) then
       e_nx_   = q+1
       e_imin_ = e_imin + (iproc-1)*(q+1)
    else
       e_nx_   = q
       e_imin_ = e_imin + r*(q+1) + (iproc-r-1)*q
    end if
    e_imax_  = e_imin_ + e_nx_ - 1
    e_imino_ = e_imin_ - e_nover
    e_imaxo_ = e_imax_ + e_nover
    ! Partitioning in Y
    q = e_ny/npy
    r = mod(e_ny,npy)
    if (jproc<=r) then
       e_ny_   = q+1
       e_jmin_ = e_jmin + (jproc-1)*(q+1)
    else
       e_ny_   = q
       e_jmin_ = e_jmin + r*(q+1) + (jproc-r-1)*q
    end if
    e_jmax_  = e_jmin_ + e_ny_ - 1
    e_jmino_ = e_jmin_ - e_nover
    e_jmaxo_ = e_jmax_ + e_nover
    ! Partitioning in Z
    q = e_nz/npz
    r = mod(e_nz,npz)
    if (kproc<=r) then
       e_nz_   = q+1
       e_kmin_ = e_kmin + (kproc-1)*(q+1)
    else
       e_nz_   = q
       e_kmin_ = e_kmin + r*(q+1) + (kproc-r-1)*q
    end if
    e_kmax_  = e_kmin_ + e_nz_ - 1
    e_kmino_ = e_kmin_ - e_nover
    e_kmaxo_ = e_kmax_ + e_nover

    ! Allocate Arrays
    allocate(e_x(e_imino:e_imaxo+1))
    allocate(e_y(e_jmino:e_jmaxo+1))
    allocate(e_z(e_kmino:e_kmaxo+1))
    allocate(e_xm (e_imino:e_imaxo))
    allocate(e_ym (e_jmino:e_jmaxo))
    allocate(e_zm (e_kmino:e_kmaxo))

    ! Compute stretched e-mesh coordiantes
    ! x coordinates
    ! Stretching around center
    do i=e_imino,e_imaxo+1
       if (stretchx.eq.0) e_x(i) = real(i-e_imin,WP)*e_Lx/real(e_nx,WP) - 0.5_WP*e_Lx
       if (stretchx.gt.0) then
          q = int(i-(1+e_nover+e_nx*0.5))/abs(i-(1+e_nover+e_nx*0.5))
          if (q.ne.q) q = 1
          e_x(i) = q*(exp(real(stretchx)*real(abs(real(i-(1.0_WP+real(e_nover,WP)+&
               real(e_nx,WP)*0.5_WP),WP)),WP)/real(e_nx,WP))-1.0_WP)
       end if
    end do
    e_x = e_x/e_x(e_imax+1)*bellrad
    ! ! Stretching from imin
    ! ! x coordinates
    ! do i=e_imino,e_imaxo+1
    !    e_x(i) = real(i-e_imin,WP)/real(e_nx,WP)&
    !         + exp(stretchx*real(i-e_imin,WP)/real(e_nx,WP))-1.0_WP
    ! end do
    ! e_x=e_Lx*e_x/e_x(e_imax+1)
    ! y coordinates
    do j=e_jmino,e_jmaxo+1
       e_y(j) = real(j-e_jmin,WP)/real(e_ny,WP)&
            + exp(stretchy*real(j-e_jmin,WP)/real(e_ny,WP))-1.0_WP
    end do
    e_y=e_Ly*e_y/e_y(e_jmax+1)
    ! z coordinates
    do k=e_kmino,e_kmaxo+1
       e_z(k) = real(k-e_kmin,WP)/real(e_nz,WP)&
            + exp(stretchz*real(k-e_kmin,WP)/real(e_nz,WP))-1.0_WP
    end do
    e_z=(e_Lz*e_z/e_z(e_kmax+1))-e_Lz*0.5

    ! Compute location of cell centers
    do i=e_imino,e_imaxo
       e_xm(i) = 0.5_WP*(e_x(i)+e_x(i+1))
    end do
    do j=e_jmino,e_jmaxo
       e_ym(j) = 0.5_WP*(e_y(j)+e_y(j+1))
    end do
    do k=e_kmino,e_kmaxo
       e_zm(k) = 0.5_WP*(e_z(k)+e_z(k+1))
    end do
    
    return
  end subroutine domain_init

  ! =============================== !
  !  Initiate Parallel Environment  !
  ! =============================== !
  subroutine parallel_init
    implicit none
    integer, dimension(3) :: dims
    logical, dimension(3) :: isper
    logical :: reorder
    integer :: ndims
    integer, dimension(3) :: coords

    call MPI_INIT(ierr)
    call MPI_COMM_RANK(mpi_comm_world,irank,ierr)
    call MPI_COMM_SIZE(mpi_comm_world,numproc,ierr)
    if(numproc.ne.npx*npy .and. irank.eq.1) then
       print*,'Incorrect Number of Processors Specified!'
       stop
    end if
    comm = mpi_comm_world
    ! Set MPI topology
    ndims = 3
    dims(1) = npx
    dims(2) = npy
    dims(3) = npz
    isper   = .false.
    reorder = .true.
    ! Create Cartesian grid for parallel computing
    call MPI_CART_CREATE(mpi_comm_world,ndims,dims,isper,reorder,comm,ierr)
    call MPI_CART_COORDS(comm,irank,ndims,coords,ierr)
    irank = irank + 1
    iproc = coords(1) + 1
    jproc = coords(2) + 1
    kproc = coords(3) + 1
    
    return
  end subroutine parallel_init

  ! ====================== !
  !  Populate data arrays  !
  ! ====================== !
  subroutine data_init
    implicit none
    
    ! Allocate solution array
    allocate(e_v(e_imino_:e_imaxo_,e_jmino_:e_jmaxo_,e_kmino_:e_kmaxo_))
    e_v=0.0_WP
    
    return
  end subroutine data_init
  
  ! =============== !
  !  INITIATE SILO  !
  ! =============== !
  subroutine silo_init

    if (irank.eq.1) call execute_command_line('rm Visit.silo')
    
    return
  end subroutine silo_init

  ! ==================== !
  !  WRITE TO SILO FILE  !
  ! ==================== !
  subroutine silo_write
    implicit none
    integer :: dbfile, ierr
    integer :: proccount
    integer,dimension(numproc) :: lnames,types
    character(len=50) :: siloname,dirname
    character(len=20),dimension(numproc) :: names

    ! Create the silo database
    write(siloname,'(A)') 'Visit.silo'
    if (irank.eq.1)then
       ierr = dbcreate(siloname,len_trim(siloname),DB_CLOBBER,DB_LOCAL,"Silo database",13,DB_HDF5,dbfile)
       if(dbfile.eq.-1) print *,'Could not create Silo file!'
       ierr = dbclose(dbfile)
    end if
    call MPI_BARRIER(MPI_COMM_WORLD,ierr)
    
    ! Each processor writes its data
    do proccount=1,numproc
       if(irank.eq.proccount)then
          ierr = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)
          write(dirname,'(I5.5)') irank
          ierr = dbmkdir(dbfile,dirname,5,ierr)
          ierr = dbsetdir(dbfile,dirname,5)
          ierr = dbputqm(dbfile,"e_Mesh",6,'xc',2,'yc',2,'zc',2,&
               e_x(e_imin_:e_imax_+1),e_y(e_jmin_:e_jmax_+1),e_z(e_kmin_:e_kmax_+1),&
               (/e_nx_+1,e_ny_+1,e_nz_+1/),3,DB_DOUBLE,DB_COLLINEAR,DB_F77NULL,ierr)
          ierr = dbputqv1(dbfile,'e_v',3,"e_Mesh",6,e_v(e_imin_:e_imax_,e_jmin_:e_jmax_,e_kmin_:e_kmax_),&
               (/e_nx_,e_ny_,e_nz_/),3,DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
          ierr = dbclose(dbfile)
       end if
       call MPI_BARRIER(MPI_COMM_WORLD,ierr)
    end do
    
    ! Write Multimesh/Multivars
    if (irank.eq.1) then
       ierr = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)
       ierr = dbset2dstrlen(20)
       ! e_Mesh
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount,'/e_Mesh'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUAD_RECT
       end do
       names = ["00001/e_Mesh","00002/e_Mesh"]
       lnames = 12
       types=DB_QUAD_RECT
       ierr = dbputmmesh(dbfile,"e_Mesh",6,numproc,names,lnames,types,DB_F77NULL,ierr)
       e_v
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount,'/e_v'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUADVAR
       end do
       ierr = dbputmvar(dbfile,"e_v",3,numproc,names,lnames,types,DB_F77NULL,ierr)
       ierr = dbclose(dbfile)
    end if
    
    return
  end subroutine silo_write

  ! ================= !
  !  Read input data  !
  ! ================= !
  subroutine read_input
    implicit none
    
    ! Domain geometry
    e_nx = 10
    e_ny = 10
    e_nz = 10
    e_Lx = 0.2_WP
    e_Ly = 0.25_WP
    e_Lz = 0.1_WP
    bellrad = 0.1
    stretchx = 5
    stretchy = 5
    stretchz = 0
    periodicity = [0,0,1]
    ! Partitioning
    npx = 2
    npy = 1
    npz = 1
    ! Dirichlet BC values
    phi_imin = 0.0_WP
    phi_imax = 0.0_WP
    phi_jmin = 0.0_WP
    phi_jmax = 0.0_WP
    phi_kmin = 0.0_WP
    phi_kmax = 0.0_WP
    phi_bell = 80000.0_WP
    
    return
  end subroutine read_input

end program hypre_test
