HDF5_DIR = /Users/venkatkrisshna/opt/hdf5
SZIP_DIR = /Users/venkatkrisshna/opt/szip
SILO_DIR = /Users/venkatkrisshna/opt/silo

HYPRE_DIR = /Users/venkatkrisshna/opt/hypre
HYPRE_INC = -I$(HYPRE_DIR)/include
HYPRE_LIB = -L$(HYPRE_DIR)/lib -lHYPRE

SILO_INC = -I$(SILO_DIR)/include
SILO_LIB = -L$(SILO_DIR)/lib -lsiloh5 -L$(HDF5_DIR)/lib -lhdf5 -L$(SZIP_DIR)/lib -lsz -lz -lstdc++ #-L/usr/local/lib -lstdc++ -lz

C_SRC := $(wildcard *.c)
F_SRC := $(wildcard *.f90)

F90 := mpifort #gfortran
CC  := mpicc   #gcc

FFLAGS := -g -fbounds-check -Wall -pedantic -fbacktrace

OBJECTS := $(C_SRC:%.c=%.o) $(F_SRC:%.f90=%.o)

run: silo_writeRead
	        mpirun -np 2 ./hypre_test

silo_writeRead: $(OBJECTS)
	        $(F90) -o hypre_test *.o -lc $(SILO_LIB) $(HYPRE_LIB)

%.o: %.c
	        $(CC) $(SILO_INC) -c $< -o $@

%.o: %.f90
	        $(F90) $(FFLAGS) $(SILO_INC) $(HYPRE_INC) -c $< -o $@

clean:
	        rm -fr *.o hypre_test Visit
